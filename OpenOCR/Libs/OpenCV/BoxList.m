//
// Created by magnus on 2013-03-20.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <CoreGraphics/CoreGraphics.h>
#import "BoxList.h"

#define MAX_DISTANCE_X 0
#define MAX_DISTANCE_Y 0

#define MIN_CHARACTER_WIDTH 0
#define MIN_CHARACTER_HEIGHT 0
#define MAX_CHARACTER_WIDTH 200
#define MAX_CHARACTER_HEIGHT 200

#define MIN_CHARACTER_RATIO 0
#define MAX_CHARACTER_RATIO 200

@implementation BoxList
{
	NSMutableArray *array;
	NSMutableArray *inputs;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		array = [NSMutableArray array];
		inputs = [NSMutableArray array];
	}

	return self;
}

- (NSArray *)values
{
	return array;
}

- (NSArray *)inputsRects
{
	return inputs;
}

- (NSArray *)combinedRects
{
	NSMutableArray *result = [NSMutableArray array];

	for(NSMutableArray *subarr in array)
	{
		CGRect rect = [[subarr objectAtIndex:0] CGRectValue];

		for(NSValue *value in subarr)
		{
			rect = CGRectUnion([value CGRectValue], rect);
		}

		float ratio = rect.size.height / rect.size.width;


		if(rect.size.width >= MIN_CHARACTER_WIDTH && rect.size.height >= MIN_CHARACTER_HEIGHT &&
				rect.size.width <= MAX_CHARACTER_WIDTH && rect.size.height <= MAX_CHARACTER_HEIGHT &&
				ratio >= MIN_CHARACTER_RATIO && ratio <= MAX_CHARACTER_RATIO)
		{
			//NSLog(@"%@", NSStringFromCGRect(rect));
			[result addObject:[NSValue valueWithCGRect:rect]];
		}

	}

	return [result sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2)
	{
		CGRect rect1 = [obj1 CGRectValue];
		CGRect rect2 = [obj2 CGRectValue];

		if (rect1.origin.x > rect2.origin.x)
			return NSOrderedDescending;
		else if (rect1.origin.x < rect2.origin.x)
			return NSOrderedAscending;
		return NSOrderedSame;
	}];
}

- (void)addRect:(CGRect)rect
{

	BOOL foundNeighbour = NO;

	for(NSMutableArray *subarr in array)
	{
		for(NSValue *value in subarr)
		{
			CGRect r = [value CGRectValue];

			if([self isNeighbours:r and:rect])
			{
				[subarr addObject:[NSValue valueWithCGRect:rect]];
				foundNeighbour = YES;
				break;
			}
		}

		if(foundNeighbour)
		{
			break;
		}
	}

	if(!foundNeighbour)
	{
		[array addObject:[NSMutableArray arrayWithObject:[NSValue valueWithCGRect:rect]]];
	}

	[inputs addObject:[NSValue valueWithCGRect:CGRectInset(rect, -MAX_DISTANCE_X, -MAX_DISTANCE_Y)]];



	/*
	CGRect newRect = CGRectUnion(r, rect);
				newValue = [NSValue valueWithCGRect:newRect]
	 */

	/*
	if(newValue)
	{
		[array replaceObjectAtIndex:[array indexOfObject:oldValue] withObject:newValue];
	}
	else
	{
		[array addObject:[NSValue valueWithCGRect:rect]];
	}
	*/
}

- (BOOL)isNeighbours:(CGRect)rect1 and:(CGRect)rect2
{
	CGRect new1 = CGRectInset(rect1, -MAX_DISTANCE_X, -MAX_DISTANCE_Y);
	CGRect new2 = CGRectInset(rect2, -MAX_DISTANCE_X, -MAX_DISTANCE_Y);

	return CGRectIntersectsRect(new1, new2);
}

- (CGSize)distanceBetween:(CGRect)rect1 and:(CGRect)rect2
{
	if (CGRectIntersectsRect(rect1, rect2))
	{
		return CGSizeMake(0, 0);
	}

	CGRect mostLeft = rect1.origin.x < rect2.origin.x ? rect1 : rect2;
	CGRect mostRight = rect2.origin.x < rect1.origin.x ? rect1 : rect2;

	CGFloat xDifference = mostLeft.origin.x == mostRight.origin.x ? 0 : mostRight.origin.x - (mostLeft.origin.x + mostLeft.size.width);
	xDifference = MAX(0, xDifference);

	CGRect upper = rect1.origin.y < rect2.origin.y ? rect1 : rect2;
	CGRect lower = rect2.origin.y < rect1.origin.y ? rect1 : rect2;

	CGFloat yDifference = upper.origin.y == lower.origin.y ? 0 : lower.origin.y - (upper.origin.y + upper.size.height);
	yDifference = MAX(0, yDifference);

	return CGSizeMake(xDifference, yDifference);
}

@end