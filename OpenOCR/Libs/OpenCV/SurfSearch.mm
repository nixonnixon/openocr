//
// Created by magnus on 2013-03-21.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "SurfSearch.h"
#import <CoreVideo/CoreVideo.h>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/legacy/compat.hpp>
#import "findObjOpenCV.h"
#import "UIImageCVMatConverter.h"

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <iostream>
#include <vector>

@implementation SurfSearch
{

}

+ (NSString *)identifyImage:(IplImage *)image
{
	cv::Mat image_copy;
	cv::Mat imgMat(image);
	cv::cvtColor(imgMat, image_copy, CV_RGB2GRAY);

	IplImage wtfJump = image_copy;

	if([self object:&wtfJump isImage:@"0.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"1.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"2.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"A.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"E.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"I.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"R.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"V.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"W.png"])
	{
		return @"0";
	}

	return nil;
}

+ (BOOL)object:(IplImage *)object isImage:(NSString *)imageNamed
{
	IplImage templateObject = [UIImageCVMatConverter cvMatGrayFromUIImage:[UIImage imageNamed:imageNamed]];

	return [self findObject:&templateObject inImage:object];
}

+ (BOOL)findObject:(IplImage *)object inImage:(IplImage *)image
{
	if( !object || !image )
	{
		//NSLog(@"Missing object or image");
		return nil;
	}

	//NSLog(@"%@ %@", self, NSStringFromSelector(_cmd));
	cv::initModule_nonfree();
	CvMemStorage* storage = cvCreateMemStorage(0);
	static CvScalar colors[] =
			{
					{{0,0,255}},
					{{0,128,255}},
					{{0,255,255}},
					{{0,255,0}},
					{{255,128,0}},
					{{255,255,0}},
					{{255,0,0}},
					{{255,0,255}},
					{{255,255,255}}
			};


	CvSeq *objectKeypoints = 0, *objectDescriptors = 0;
	CvSeq *imageKeypoints = 0, *imageDescriptors = 0;
	int i;
	CvSURFParams params = cvSURFParams(10, 1);

	double tt = (double)cvGetTickCount();
	//NSLog(@"Finding object descriptors");
	cvExtractSURF(object, 0, &objectKeypoints, &objectDescriptors, storage, params );

	NSLog(@"Object Descriptors: %d", objectDescriptors->total);
	cvExtractSURF( image, 0, &imageKeypoints, &imageDescriptors, storage, params );

	NSLog(@"Image Descriptors: %d", imageDescriptors->total);
	tt = (double)cvGetTickCount() - tt;

	//NSLog(@"Extraction time = %gms", tt/(cvGetTickFrequency()*1000.));
	//CvPoint src_corners[4] = {{0,0}, {object->width,0}, {object->width, object->height}, {0, object->height}};
	//CvPoint dst_corners[4];

	//NSLog(@"Locating Planar Object");
#ifdef USE_FLANN
 	//NSLog(@"Using approximate nearest neighbor search");
#endif

	vector<int> ptpairs;
	//NSLog(@"finding Pairs");
#ifdef USE_FLANN
    flannFindPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#else
	findPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#endif


	//NSLog(@"found pairs: %d", (int) ptpairs.size());

	return ptpairs.size() > 0;
}

@end