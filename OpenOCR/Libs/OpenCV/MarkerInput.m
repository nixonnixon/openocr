//
// Magnus Ottosson [magnus@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-18 15:51
//
#import "MarkerInput.h"


@implementation MarkerInput
{

}

+ (MarkerInput *)inputWithCharacter:(NSString *)character imageNamed:(NSString *)imageName
{
	MarkerInput *input = [[MarkerInput alloc] init];
	[input setCharacter:character];
	[input setImageName:imageName];

	return input;
}

@end