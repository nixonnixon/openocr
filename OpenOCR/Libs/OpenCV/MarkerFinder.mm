//
// Magnus Ottosson [magnus@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-18 12:12
//
#import "MarkerFinder.h"
#import "UIImageCVMatConverter.h"
#import "MarkerMatch.h"
#import "MarkerInput.h"
#import <queue>

@implementation MarkerFinder
{

}

+ (NSString *)findMarkers:(NSArray *)markers inImage:(UIImage *)image
{
	return [self findMarkers:markers inMat:[UIImageCVMatConverter cvMatFromUIImage:image]];
}

+ (NSString *)findInMat:(cv::Mat)image
{
	NSArray *inputs = [NSArray arrayWithObjects:[MarkerInput inputWithCharacter:@"0" imageNamed:@"0.png"],
												[MarkerInput inputWithCharacter:@"1" imageNamed:@"1.png"],
												[MarkerInput inputWithCharacter:@"2" imageNamed:@"2.png"],
												[MarkerInput inputWithCharacter:@"A" imageNamed:@"A.png"],
												[MarkerInput inputWithCharacter:@"E" imageNamed:@"E.png"],
												[MarkerInput inputWithCharacter:@"I" imageNamed:@"I.png"],
												[MarkerInput inputWithCharacter:@"R" imageNamed:@"R.png"],
												[MarkerInput inputWithCharacter:@"V" imageNamed:@"V.png"],
												[MarkerInput inputWithCharacter:@"W" imageNamed:@"W.png"], nil];

	return [self findMarkers:inputs inMat:image];
}

+ (NSString *)findMarkers:(NSArray *)markers inMat:(cv::Mat)image
{
	cv::Mat src_img;
	cv::Mat result_mat;
	//cv::Mat debug_img;
	cv::Mat template_img;

	// input image
	src_img = [self convertToEdges:image];
	//src_img = [self convertToEdges:image];
	//src_img = [UIImageCVMatConverter cvMatFromUIImage:image];

	//cv::cvtColor(src_img, debug_img, CV_BGRA2BGR);

	NSMutableArray *matches = [NSMutableArray array];

	MarkerMatch *best;

	for (MarkerInput *input in markers)
	{
		template_img = [self convertToEdges:[UIImageCVMatConverter cvMatFromUIImage:[UIImage imageNamed:input.imageName]]];
		//template_img = [UIImageCVMatConverter cvMatFromUIImage:[UIImage imageNamed:input.imageName]];
		//cv::cvtColor(template_img, template_img, CV_GRAY2BGR);

		int match_method = CV_TM_CCORR_NORMED;

		cv::matchTemplate(src_img, template_img, result_mat, match_method);

		//cv::normalize(result_mat, result_mat, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

		/*
		double minVal;
		double maxVal;

		cv::Point minLoc, maxLoc, matchLoc;
		cv::minMaxLoc(result_mat, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );
		if ( match_method  == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED ) {
			matchLoc = minLoc;
		}
		else {
			matchLoc = maxLoc;
		}

		cv::Point top_left = matchLoc;
		cv::Point bottom_right = cv::Point(matchLoc.x + template_img.cols , matchLoc.y + template_img.rows);
		cv::Point center = cv::Point(0,0);

		center.x = (bottom_right.x + top_left.x) / 2;
		center.y = (bottom_right.y + top_left.y) / 2;

		markerPoints.push_back(center);
		*/

		NSMutableArray *characterMatches = [NSMutableArray array];


		for (MarkerMatch *match in [self matchesInResult:result_mat size:1 minScore:0.6])
		{
			match.input = input;
			/*
			match.input = input;

			//NSLog(@"%@ %f %f", match.input.character, match.x, match.y);

			BOOL found = NO;

			for (MarkerMatch *tempMatch in characterMatches)
			{
				if (tempMatch.x > match.x - 5 && tempMatch.x < match.x + 5)
				{
					found = YES;
				}
			}

			if (!found)
			{
				[characterMatches addObject:match];
			}
			*/

			if (!best)
			{
				best = match;
				//NSLog(@"char: %@ score: %f", match.input.character, match.score);
			}
			else if (best.score < match.score)
			{
				best = match;
				//NSLog(@"char: %@ score: %f", match.input.character, match.score);
			}
		}

		//[matches addObjectsFromArray:characterMatches];
	}

	if(best)
	{
		return best.input.character;
	}

	return nil;

	//NSLog(@"char: %@ score: %f", best.input.character, best.score);

	/*
	[matches sortUsingComparator:^NSComparisonResult(MarkerMatch *match1, MarkerMatch *match2)
	{
		if (match1.x > match2.x)
			return NSOrderedDescending;
		else if (match1.x < match2.x)
			return NSOrderedAscending;
		return NSOrderedSame;
	}];

	NSString *code = @"";

	for (MarkerMatch *match in matches)
	{
		code  = [NSString stringWithFormat:@"%@%@", code, match.input.character];
	}
	*/

	/*
	if (matches.count == 10)
	{
		NSLog(@"Result: %@", code);
	}
	else
	{
		NSLog(@"count: %d", matches.count);
	}
	*/

}

+ (cv::Mat)convertToEdges:(cv::Mat)src
{
	cv::Mat out;
	cv::Mat src_gray;
	cv::Mat grad;
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;

	//cv::GaussianBlur(src, out, Size(3,3), 0, 0, cv::BORDER_DEFAULT);

	/// Convert it to gray
	cv::cvtColor(src, src_gray, CV_RGB2GRAY );

	/// Generate grad_x and grad_y
	cv::Mat grad_x, grad_y;
	cv::Mat abs_grad_x, abs_grad_y;

	/// Gradient X
	//Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
	cv::Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );
	cv::convertScaleAbs( grad_x, abs_grad_x );

	/// Gradient Y
	//Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
	cv::Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );
	cv::convertScaleAbs( grad_y, abs_grad_y );

	/// Total Gradient (approximate)
	cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

	return grad;
}

+ (NSArray *)matchesInResult:(cv::Mat)result size:(NSUInteger)size minScore:(float)minScore
{
	float *srcData = (float *) result.data;

	NSMutableArray *matches = [NSMutableArray arrayWithCapacity:size];

	for(int i = 0; i < result.rows; i++)
	{
		for(int j = 0; j < result.cols; j++)
		{
			float score = srcData[i*result.cols + j];

			if (score > minScore)
			{
				[matches addObject:[MarkerMatch matchWithX:j Y:i score:score]];
			}

		}
	}

	NSArray *sorted = [matches sortedArrayUsingComparator:^NSComparisonResult(MarkerMatch *match1, MarkerMatch *match2)
	{
		if (match1.score > match2.score)
			return NSOrderedAscending;
		else if (match1.score < match2.score)
			return NSOrderedDescending;
		return NSOrderedSame;
	}];

	if (sorted.count <= size)
	{
		return sorted;
	}

	return [sorted subarrayWithRange:NSMakeRange(0, size)];
}
@end