//
// Magnus Ottosson [magnus@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-18 13:59
//
#import "MarkerMatch.h"
#import "MarkerInput.h"


@implementation MarkerMatch
{

}

+ (MarkerMatch *)matchWithX:(float)x Y:(float)y score:(float)score
{
	MarkerMatch *match = [[MarkerMatch alloc] init];
	[match setX:x];
	[match setY:y];
	[match setScore:score];

	return match;
}

@end