//
// Created by magnus on 2013-03-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "PixelMatch.h"


@implementation PixelMatch
{

}

+ (BOOL)object:(cv::Mat)object matchesWith:(cv::Mat)object2
{
	cv::Mat result;

	int threshold = (int) ((object.rows * object.cols) * 0.8);

	cv::compare(object , object2, result, cv::CMP_EQ);
	int similarPixels  = countNonZero(result);

	return similarPixels >= threshold;
}
@end