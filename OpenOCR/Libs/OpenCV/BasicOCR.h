//
// Created by magnus on 2013-03-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface BasicOCR : NSObject


+ (BasicOCR *)instance;

- (NSString *)identifyImage:(IplImage *)image;
@end