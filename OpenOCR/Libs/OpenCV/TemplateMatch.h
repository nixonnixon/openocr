//
// Created by magnus on 2013-03-21.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface TemplateMatch : NSObject
+ (NSString *)identifyImage:(IplImage *)image;

+ (float)object:(cv::Mat)object matchesWith:(cv::Mat)object2;
@end