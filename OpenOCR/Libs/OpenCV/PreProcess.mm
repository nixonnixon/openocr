//
// Created by magnus on 2013-03-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "PreProcess.h"

using namespace cv;


@implementation PreProcess
{

}

+ (cv::Mat)processImage:(IplImage *)image
{
	cv::Mat searchDigit(image);

	cv::Mat image_copy;
	searchDigit.copyTo(image_copy);

	cv::resize(image_copy, image_copy, cv::Size(10, 10), 0, 0, cv::INTER_NEAREST);

	[self convertToBW:image_copy];

	return image_copy;
}

+ (void)convertToBW:(cv::Mat&)src
{
	cv::cvtColor(src,src,CV_RGB2GRAY);
	cv::GaussianBlur(src,src,cv::Size(5,5),0);
	cv::adaptiveThreshold(src, src, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 7, 4);
}

+ (cv::Mat)convertToEdges:(cv::Mat)src
{
	cv::Mat out;
	cv::Mat src_gray;
	cv::Mat grad;
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;

	//cv::GaussianBlur(src, out, Size(3,3), 0, 0, cv::BORDER_DEFAULT);

	/// Convert it to gray
	cv::cvtColor(src, src_gray, CV_RGB2GRAY );

	/// Generate grad_x and grad_y
	cv::Mat grad_x, grad_y;
	cv::Mat abs_grad_x, abs_grad_y;

	/// Gradient X
	//Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
	cv::Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );
	cv::convertScaleAbs( grad_x, abs_grad_x );

	/// Gradient Y
	//Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
	cv::Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );
	cv::convertScaleAbs( grad_y, abs_grad_y );

	/// Total Gradient (approximate)
	cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

	return grad;
}

@end