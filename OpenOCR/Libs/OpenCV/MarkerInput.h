//
// Magnus Ottosson [magnus@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-18 15:51
//
#import <Foundation/Foundation.h>


@interface MarkerInput : NSObject

@property(nonatomic, retain) NSString *character;
@property(nonatomic, retain) NSString *imageName;

+ (MarkerInput *)inputWithCharacter:(NSString *)character imageNamed:(NSString *)imageName;
@end