//
// Magnus Ottosson [magnus@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-18 13:59
//
#import <Foundation/Foundation.h>

@class MarkerInput;


@interface MarkerMatch : NSObject

@property(nonatomic) float x;
@property(nonatomic) float y;
@property(nonatomic) float score;
@property(nonatomic, retain) MarkerInput *input;

+ (MarkerMatch *)matchWithX:(float)x Y:(float)y score:(float)score;
@end