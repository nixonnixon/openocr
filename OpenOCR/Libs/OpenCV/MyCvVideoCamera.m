//
// Mikael Bartlett [mikael@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-19 09:17
//

#import "MyCvVideoCamera.h"


@implementation MyCvVideoCamera

- (void)updateOrientation;
{
	// nop
}
- (void)layoutPreviewLayer;
{
	if (self.parentView != nil) {
		CALayer* layer = self.customPreviewLayer;
		CGRect bounds = self.customPreviewLayer.bounds;
		layer.position = CGPointMake(self.parentView.frame.size.width/2., self.parentView.frame.size.height/2.);
		layer.bounds = bounds;
	}
}
@end