//
// Created by magnus on 2013-03-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface PreProcess : NSObject

+ (cv::Mat)processImage:(IplImage *)image;
+ (void)convertToBW:(cv::Mat&)src;

@end