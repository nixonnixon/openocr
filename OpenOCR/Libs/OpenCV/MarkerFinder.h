//
// Magnus Ottosson [magnus@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-18 12:12
//
#import <Foundation/Foundation.h>
#import <opencv2/core/core.hpp>


@interface MarkerFinder : NSObject

+ (NSString *)findMarkers:(NSArray *)markers inImage:(UIImage *)image;

#ifdef __cplusplus
+ (NSString *)findInMat:(cv::Mat)image;
#endif

@end