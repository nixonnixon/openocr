//
// Mikael Bartlett [mikael@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-19 09:17
//

#import <Foundation/Foundation.h>
#import <opencv2/highgui/cap_ios.h>


@interface MyCvVideoCamera : CvVideoCamera
- (void)updateOrientation;
- (void)layoutPreviewLayer;

@property (nonatomic, retain) CALayer *customPreviewLayer;

@end