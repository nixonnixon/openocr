//
// Created by magnus on 2013-03-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface PixelMatch : NSObject
+ (BOOL)object:(cv::Mat)object matchesWith:(cv::Mat)object2;
@end