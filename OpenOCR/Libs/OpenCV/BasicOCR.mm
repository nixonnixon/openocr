//
// Created by magnus on 2013-03-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "BasicOCR.h"
#import "UIImageCVMatConverter.h"
#import "PreProcess.h"

#define SAMPLE_COUNT 1
#define SIZE 10
#define K 10

@implementation BasicOCR
{
	CvMat* trainData;
	CvMat* trainClasses;
	CvMat row, data;

	NSArray *imageLabels;
	CvKNearest *knn;
}

+ (BasicOCR *)instance
{
	static BasicOCR *instance;
	static dispatch_once_t pred;

	dispatch_once(&pred, ^{
		instance = [[BasicOCR alloc] init];
	});

	return instance;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		imageLabels = [NSArray arrayWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"7", @"8", @"9", @"a", @"c", @"e", @"i", @"j", @"k", @"n", @"p", @"r", @"t", @"u", @"v", @"w", @"y", nil];

		[self loadTestData];
	}

	return self;
}


- (void)loadTestData
{
	trainData = cvCreateMat(SAMPLE_COUNT * imageLabels.count, SIZE * SIZE, CV_32FC1);
	trainClasses = cvCreateMat(SAMPLE_COUNT * imageLabels.count, 1, CV_32FC1);

	for(NSString *label in imageLabels)
	{
		[self trainCharacter:label];
	}

	knn = new CvKNearest( trainData, trainClasses, 0, false, K);
}

- (NSString *)identifyImage:(IplImage *)image
{
	float result;
	CvMat* nearest = cvCreateMat(1, K, CV_32FC1);
	cv::Mat processedTemplate = [PreProcess processImage:image];

	IplImage prs_image = processedTemplate;

	CvMat data;
	CvMat row_header, *row1;

	//Set data
	IplImage* img32 = cvCreateImage(cvSize(SIZE, SIZE), IPL_DEPTH_32F, 1);
	cvConvertScale(&prs_image, img32, 0.0039215, 0);
	cvGetSubRect(img32, &data, cvRect(0,0, SIZE,SIZE));
	row1 = cvReshape( &data, &row_header, 0, 1 );

	result = knn->find_nearest(row1, K, 0, 0, nearest, 0);

	int accuracy=0;
	for(int i = 0; i < K; i++)
	{
		if(nearest->data.fl[i] == result)
		{
			accuracy++;
		}
	}

	NSString *resultCharacter = [imageLabels objectAtIndex:(NSUInteger) result];

	float pre=100*((float)accuracy/(float)K);

	//NSLog(@"%@ | %.0f | %.2f | %d of %d", resultCharacter, result, pre, accuracy, K);
	//printf(" ---------------------------------------------------------------\n");

	return resultCharacter;
}

- (void)trainCharacter:(NSString *)character
{
	int i = [imageLabels indexOfObject:character];
	int j = 0;


	for(int k = 1; k <= SAMPLE_COUNT; k++)
	{
		NSString *imageName = [NSString stringWithFormat:@"%@-%d.png", character, k];

		IplImage templateObject = [UIImageCVMatConverter cvMatFromUIImage:[UIImage imageNamed:imageName]];
		cv::Mat processedTemplate = [PreProcess processImage:&templateObject];
		IplImage prs_image = processedTemplate;

		//Set class label
		cvGetRow(trainClasses, &row, i * SAMPLE_COUNT + j);
		cvSet(&row, cvRealScalar(i));
		//Set data
		cvGetRow(trainData, &row, i * SAMPLE_COUNT + j);

		IplImage* img = cvCreateImage(cvSize(SIZE, SIZE), IPL_DEPTH_32F, 1 );
		//convert 8 bits image to 32 float image
		cvConvertScale(&prs_image, img, 0.0039215, 0);

		cvGetSubRect(img, &data, cvRect(0,0, SIZE,SIZE));

		CvMat row_header, *row1;
		//convert data matrix sizexsize to vecor
		row1 = cvReshape( &data, &row_header, 0, 1 );
		cvCopy(row1, &row, NULL);

		j++;
	}
}

@end