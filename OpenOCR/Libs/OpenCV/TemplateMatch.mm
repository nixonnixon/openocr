//
// Created by magnus on 2013-03-21.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "TemplateMatch.h"
#import "UIImageCVMatConverter.h"


@implementation TemplateMatch
{

}

+ (NSString *)identifyImage:(IplImage *)image
{
	cv::Mat image_copy;
	cv::Mat imgMat(image);
	cv::cvtColor(imgMat, image_copy, CV_RGB2GRAY);

	IplImage wtfJump = image_copy;

	/*
	if([self object:&wtfJump isImage:@"0.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"1.png"])
	{
		return @"0";
	}
	*/

	if([self object:&wtfJump  isImage:@"2.png"])
	{
		return @"0";
	}

	/*
	if([self object:&wtfJump  isImage:@"A.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"E.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"I.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"R.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"V.png"])
	{
		return @"0";
	}

	if([self object:&wtfJump  isImage:@"W.png"])
	{
		return @"0";
	}
	*/

	return nil;
}

+ (BOOL)object:(IplImage *)object isImage:(NSString *)imageNamed
{
	IplImage templateObject = [UIImageCVMatConverter cvMatGrayFromUIImage:[UIImage imageNamed:imageNamed]];

	return [self findObject:&templateObject inImage:object];
}

+ (BOOL)findObject:(IplImage *)object inImage:(IplImage *)image
{

	return NO;
}



+ (float)object:(cv::Mat)object matchesWith:(cv::Mat)object2
{
	float min = 0.8;
	float bestScore = 0.0f;
	cv::Mat result_mat;
	int match_method = CV_TM_CCORR_NORMED;

	cv::matchTemplate(object, object2, result_mat, match_method);

	float *srcData = (float *) result_mat.data;

	for(int i = 0; i < result_mat.rows; i++)
	{
		for(int j = 0; j < result_mat.cols; j++)
		{
			float score = srcData[i*result_mat.cols + j];

			if (score > bestScore)
			{
				bestScore = score;
			}
		}
	}

	return bestScore;
}

@end