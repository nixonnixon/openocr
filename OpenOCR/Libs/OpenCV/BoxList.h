//
// Created by magnus on 2013-03-20.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface BoxList : NSObject
- (NSArray *)values;

- (NSArray *)inputsRects;

- (NSArray *)combinedRects;

- (void)addRect:(CGRect)rect;
@end