//
// Mikael Bartlett [mikael@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-01 15:15
//

#import <Foundation/Foundation.h>
#import <opencv2/highgui/cap_ios.h>


@interface CameraViewController : UIViewController<CvVideoCameraDelegate>

@end