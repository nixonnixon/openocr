//
// Mikael Bartlett [mikael@nixonnixon.se] / NIXON NIXON
// Date: 2013-03-01 15:15
//

#import "CameraViewController.h"
#import "MyCvVideoCamera.h"
#import "MarkerFinder.h"
#import "BoxList.h"
#import "UIImageCVMatConverter.h"
#import "SurfSearch.h"
#import "TemplateMatch.h"
#import "PixelMatch.h"
#import "BasicOCR.h"
#import "PreProcess.h"

using namespace cv;


@interface CameraViewController ()
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) MyCvVideoCamera *videoCamera;
@end

@implementation CameraViewController
{
	int imageCount;
}

- (id)init
{
	self = [super init];

	if(self)
	{
		imageCount = 0;
	}

	return self;
}

- (void)viewDidLoad
{
	self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];

	CGRect rect = CGRectMake(0, 0, 480, 640);
	self.imageView = [[UIImageView alloc] initWithFrame:rect];
	[self.view addSubview:self.imageView];

	[self.imageView setCenter:self.view.center];

	self.videoCamera = [[MyCvVideoCamera alloc] initWithParentView:self.imageView];
	self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
	self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset352x288;
	self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
	self.videoCamera.defaultFPS = 30;
	self.videoCamera.delegate = self;
	self.videoCamera.grayscaleMode = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
	[self.videoCamera start];
}

- (NSArray *)findRectangles:(Mat&)image
{
	Mat image_copy;
	image.copyTo(image_copy);
	[PreProcess convertToBW:image_copy];

	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	findContours(image_copy, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	/*
    for(int i=0; i<contours.size();i++)
    {
        cv::Rect rect = boundingRect(contours[i]);

        if(rect.height > 5 && rect.height > 5)
        {
            cv::Point pt1, pt2;
            pt1.x = rect.x;
            pt1.y = rect.y;
            pt2.x = rect.x + rect.width;
            pt2.y = rect.y + rect.height;
            // Draws the rect in the original image and show it
            rectangle(image, pt1, pt2, CV_RGB(255,0,0), 1);
        }
    }
    */

	BoxList *list = [[BoxList alloc] init];

	for(int i=0; i<contours.size();i++)
	{
		cv::Rect rect = boundingRect(contours[i]);

		if(rect.height >= 1 && rect.height >= 1)
		{
			CGRect r = CGRectMake(rect.x, rect.y, rect.width, rect.height);
			[list addRect:r];
		}
	}


	return list.combinedRects;
}

- (void)processImage:(Mat&)image
{
	Mat image_copy;
	image.copyTo(image_copy);
	IplImage frame = image_copy;
	int width = frame.width;
	int height = frame.height;

	[PreProcess convertToBW:image];

	cv::Point frameOffset, pt2;
	frameOffset.x = width/2 - 75;
	frameOffset.y = height/2 - 50;
	pt2.x = frameOffset.x + 150;
	pt2.y = frameOffset.y + 50;

	cvSetImageROI(&frame, cvRect(frameOffset.x, frameOffset.y, 150, 50));
	IplImage *searchImage = cvCreateImage(cvGetSize(&frame),(&frame)->depth,(&frame)->nChannels);
	cvCopy(&frame, searchImage, NULL);
	cvResetImageROI(&frame);

	Mat searchMat(searchImage);

	NSArray *rects = [self findRectangles:searchMat];

	NSString *code = @"";

	for(NSValue *value in rects)
	{
		CGRect rect = [value CGRectValue];

		cv::Point topLeft, bottomRight;
		topLeft.x = (int) (frameOffset.x + rect.origin.x);
		topLeft.y = (int) (frameOffset.y + rect.origin.y);
		bottomRight.x = (int) (topLeft.x + rect.size.width);
		bottomRight.y = (int) (topLeft.y + rect.size.height);

		cvSetImageROI(&frame, cvRect(topLeft.x, topLeft.y, (int) rect.size.width, (int) rect.size.height));
		IplImage *digit = cvCreateImage(cvGetSize(&frame),(&frame)->depth,(&frame)->nChannels);
		cvCopy(&frame, digit, NULL);
		cvResetImageROI(&frame);

		code = [NSString stringWithFormat:@"%@%@", code, [[BasicOCR instance] identifyImage:digit]];

		rectangle(image, topLeft, bottomRight, CV_RGB(255,255,255  ), 1);
	}

	//NSLog(@"%@", code);

	rectangle(image, frameOffset, pt2, CV_RGB(255,255,255), 1);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return NO;
}

@end
